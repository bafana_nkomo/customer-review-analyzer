package com.ikhokha.techcheck;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Processor {

    public Map<String, Integer> process(String line) {

        String regex = "((http:\\/\\/|https:\\/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_\\/\\.0-9#:?=&;,]*)?)?)";
        Pattern pattern = Pattern.compile(regex);
        Map<String, Integer> resultsMap = new HashMap<>();

        if (line.contains("?") ) {

            incOccurrence(resultsMap, "QUESTIONS");

        }
        if (pattern.matcher(line).find()) {

            incOccurrence(resultsMap, "SPAM");

        }
        if (line.length() < 15) {

            incOccurrence(resultsMap, "SHORTER_THAN_15");

        }
        else if (line.toLowerCase().contains("shaker") && line.toLowerCase().contains("mover")) {

            incOccurrence(resultsMap, "SHAKER_MENTIONS");
            incOccurrence(resultsMap, "MOVER_MENTIONS");

        }
        else if (line.toLowerCase().contains("mover")) {

            incOccurrence(resultsMap, "MOVER_MENTIONS");

        }
        else if (line.toLowerCase().contains("shaker")) {

            incOccurrence(resultsMap, "SHAKER_MENTIONS");

        }

        return resultsMap;

    }


    /**
     * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
     * @param countMap the map that keeps track of counts
     * @param key the key for the value to increment
     */
    private void incOccurrence(Map<String, Integer> countMap, String key) {

        countMap.putIfAbsent(key, 0);
        countMap.put(key, countMap.get(key) + 1);
    }
}
