package com.ikhokha.techcheck;

import com.ikhokha.techcheck.manager.CommentAnalyserManager;

public class Main {

	public static void main(String[] args) {
		try{
			CommentAnalyserManager.analyseComments();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
