package com.ikhokha.techcheck.manager;

import com.ikhokha.techcheck.CommentAnalyserIngestion;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class CommentAnalyserManager {

    /**
     * Reads all files from path
     * Spawns a thread for each file (small calculation for number of threads)
     * Add the results of analyse to a list
     * Take each map from the list and use it in addReportResults
     * Display totals for each map key
     */
    public static void analyseComments(){
        HashMap<String, Integer> totalResults = new HashMap<>();
        List<Future<Map<String, Integer>>> listResults = new ArrayList();

        File docPath = new File("docs");
        File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

        assert commentFiles != null;
        int numThreads = commentFiles.length < 10 ? commentFiles.length : commentFiles.length/Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(numThreads);

        for (File commentFile : commentFiles) {

            Callable<Map<String, Integer>> callable = new CommentAnalyserIngestion(commentFile);
            Future<Map<String, Integer>> fileResults = executor.submit(callable);
            listResults.add(fileResults);
        }

        for (Future<Map<String, Integer>> result : listResults) {
            try{

                addReportResults(result.get(), totalResults);
            } catch (InterruptedException | ExecutionException e) {

                e.printStackTrace();
            }
        }
        executor.shutdown();

        System.out.println("RESULTS\n=======");
        totalResults.forEach((k,v) -> System.out.println(k + " : " + v));

    }

    /**
     * This method adds the result counts from a source map to the target map. Sums each of the values by their key.
     * @param source the source map
     * @param target the target map
     */
    private static void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

        for (Map.Entry<String, Integer> entry : source.entrySet()) {
            if (target.containsKey(entry.getKey())) {
                target.put(entry.getKey(), entry.getValue() + target.get(entry.getKey()));
            } else {
                target.put(entry.getKey(), entry.getValue());
            }
        }
    }
}
