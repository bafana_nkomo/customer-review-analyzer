package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class CommentAnalyzer {

	private File file;

	public CommentAnalyzer(File file) {
		this.file = file;
	}

	/**
	 * Analyses a file line by line for count of specific attributes in the file text.
	 * @return hashmap of the number of occurrences of specific attributes according to their hashmap key.
	 */
	public Map<String, Integer> analyze() {

		Map<String, Integer> resultsMap;
		Map<String, Integer> finalMap = new HashMap<>();
		Processor processor = new Processor();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			String line;
			while ((line = reader.readLine()) != null) {
				resultsMap = processor.process(line);

				for (Map.Entry<String, Integer> entry : resultsMap.entrySet()) {
					if (finalMap.containsKey(entry.getKey())) {
						finalMap.put(entry.getKey(), entry.getValue() + finalMap.get(entry.getKey()));
					} else {
						finalMap.put(entry.getKey(), entry.getValue());
					}
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}

		return finalMap;

	}

}
