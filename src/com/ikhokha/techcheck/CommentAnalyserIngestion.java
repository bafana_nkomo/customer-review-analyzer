package com.ikhokha.techcheck;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

public class CommentAnalyserIngestion implements Callable {

    private CommentAnalyzer commentAnalyzer;

    public CommentAnalyserIngestion(File file){
        commentAnalyzer = new CommentAnalyzer(file);
    }

    @Override
    public Map<String, Integer> call() throws Exception {
        return commentAnalyzer.analyze();
    }
}
